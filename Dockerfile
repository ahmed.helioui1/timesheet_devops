FROM openjdk:8-jdk-alpine
EXPOSE 8082
ADD target/timesheet_devops-1.1-SNAPSHOT.jar timesheet_devops-1.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/timesheet_devops-1.1-SNAPSHOT.jar"]