package tn.esprit.spring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EntrepriseRepository;

import java.util.List;

/**
 * Created by ahelioui on 6/11/2022.
 */
@Service
public class EntrepriseServiceImpl implements IEntrepriseService {

    private static final Logger l = LogManager.getLogger(EntrepriseServiceImpl.class);

    @Autowired
    EntrepriseRepository entrepriseRepository;


    public int ajouterEntreprise(Entreprise entreprise){
        l.debug("Ajouter Enterprise");
        try {
            entrepriseRepository.save(entreprise);
        }
        catch (Exception e) {
            l.error("Errer d'ajout.", e.getMessage());
        }
        l.info("Ajout d'entreprise :"+entreprise.getName()+" avec succès");
        return entreprise.getId();
    }

    @Autowired
    DepartementRepository departementRepository;
    public int ajouterDepartement(Departement departement){
        l.debug("Ajouter Department");
        try {
            departementRepository.save(departement);
        }
        catch (Exception e) {
            l.info("L'ajout de department :"+departement.getName()+"est avec succès");        }
        return departement.getId();
    }

    @Override
    public void deleteEntrepriseByID(int entrepriseID){
        l.debug("Supression de l'enterprise");
       entrepriseRepository.delete(entrepriseRepository.findById(entrepriseID).get());
    }

    @Override
    public void deleteDepartementById(int departementId) {
        departementRepository.delete(departementRepository.findById(departementId).get());
    }

    public Entreprise getEntrepriseById(int entrepriseId) {
        return entrepriseRepository.findById(entrepriseId).get();
    }

    @Override
    public List<Entreprise> retrieveAllEnterprises() {
        return (List<Entreprise>) entrepriseRepository.findAll();
    }

}
