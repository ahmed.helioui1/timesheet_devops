package tn.esprit.spring.services;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Entreprise;

import java.util.List;

/**
 * Created by ahelioui on 6/11/2022.
 */
public interface IEntrepriseService {
     int ajouterEntreprise(Entreprise entreprise);
     void deleteEntrepriseByID(int entrepriseID);
     int ajouterDepartement(Departement departement);
     void deleteDepartementById(int departementId);
     Entreprise getEntrepriseById(int entrepriseId);
     List<Entreprise> retrieveAllEnterprises();
}