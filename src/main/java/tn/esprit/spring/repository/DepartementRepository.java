package tn.esprit.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.spring.entities.Departement;

/**
 * Created by ahelioui on 6/11/2022.
 */
@Repository
public interface DepartementRepository extends CrudRepository<Departement,Integer> {
}
