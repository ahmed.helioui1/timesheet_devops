package tn.esprit.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.spring.entities.Entreprise;

/**
 * Created by ahelioui on 6/11/2022.
 */
@Repository
public interface EntrepriseRepository extends CrudRepository<Entreprise,Integer>{
}
