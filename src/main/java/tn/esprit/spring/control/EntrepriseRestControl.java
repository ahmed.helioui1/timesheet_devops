package tn.esprit.spring.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.services.IEntrepriseService;

import java.util.List;

/**
 * Created by ahelioui on 6/11/2022.
 */
@RestController
public class EntrepriseRestControl {

    @Autowired
    IEntrepriseService ientrepriseservice;

    //http://localhost:8082/timesheet-devops/ajouterEntreprise
    @PostMapping("/ajouterEntreprise")
    @ResponseBody
    public int ajouterEntreprise(@RequestBody Entreprise Vermeg)
    {
        ientrepriseservice.ajouterEntreprise(Vermeg);
        return Vermeg.getId();
    }

    //http://localhost:8082/timesheet-devops/deleteEntrepriseByID/14
    @DeleteMapping("/deleteEntrepriseByID/{identreprise}")
    @ResponseBody
    public void deleteEntrepriseByID(@PathVariable("identreprise")int entrepriseId)
    {
        ientrepriseservice.deleteEntrepriseByID(entrepriseId);
    }

     //http://localhost:8082/timesheet-devops/getEntrepriseById/1
    @GetMapping(value = "getEntrepriseById/{identreprise}")
    @ResponseBody
    public Entreprise getEntrepriseById(@PathVariable("identreprise") int entrepriseId)
    {
        return ientrepriseservice.getEntrepriseById(entrepriseId);
    }

    //http://localhost:8082/timesheet-devops/findAllEntreprises
    @GetMapping(value = "findAllEntreprises")
    @ResponseBody
    public List<Entreprise> findAllEntreprises()
    {
        return ientrepriseservice.retrieveAllEnterprises();
    }


}
