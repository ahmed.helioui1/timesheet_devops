package tn.esprit.spring.control;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.services.IMissionService;

@RestController
public class MissionController {
	

    @Autowired
    IMissionService missionservice;
    
    //http://localhost:8082/timesheet-devops/addMission
    @PostMapping("/addMission")
    @ResponseBody
    public int ajouterMission(@RequestBody Mission chronos)
    {
    	missionservice.addMission(chronos);
        return chronos.getId();
    }

    //http://localhost:8082/timesheet-devops/deleteMission/14
    @DeleteMapping("/deleteMission/{missionId}")
    @ResponseBody
    public void deleteMission(@PathVariable("missionId")int missionId)
    {
    	missionservice.deleteMission(missionId);
    }

     //http://localhost:8082/timesheet-devops/getMissionById/1
    @GetMapping(value = "getMissionById/{missionId}")
    @ResponseBody
    public Mission getMissionById(@PathVariable("missionId") int missionId)
    {
        return missionservice.retrieveMission(missionId);
    }

    //http://localhost:8082/timesheet-devops/getAllMission
    @GetMapping(value = "findAllMission")
    @ResponseBody
    public List<Mission> getAllMission()
    {
        return missionservice.retrieveAllMissions();
    }

}
